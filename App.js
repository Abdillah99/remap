import React, {Component} from 'react';
import {View, Text} from 'react-native';

import { PostingStack } from './app/systems/Routers';


export default class App extends Component {
    constructor ( props ){
        
        super(props);

        this.state = {
            frame: <PostingStack/>
        };

    }
    render () {

        const Layout = PostingStack;
        return <Layout/>
  }
}
