var { StyleSheet, Platform } = require('react-native');

module.exports = Object.assign({}, StyleSheet.create({
    PoppinRegular:{
        fontFamily: 'Poppins-Regular',
    },

    PoppinBold:{
        fontFamily: 'Poppins-Bold',
    },

    PoppinLight:{
        fontFamily: 'Poppins-Light',
    },

    PoppinThin:{
        fontFamily: 'Poppins-Thin',
    },

    PoppinSemiBold:{
        fontFamily: 'Poppins-SemiBold',
    },


}));