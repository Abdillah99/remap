import React, { Component } from 'react';
import {  View, Text, } from 'react-native';

//Views
import PostingFormScreenPreview from '../../../views/Posting/Form/PostingFormScreenPreview';

export default class PostingForm extends Component {
    constructor (props) {
        
        super (props);
    
    }

    render () {

        return <PostingFormScreenPreview navigation={this.props.navigation} />

    }
}
