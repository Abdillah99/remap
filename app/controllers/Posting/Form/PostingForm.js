import React, { Component } from 'react';
import {  View, Text, } from 'react-native';

//Views
import PostingFormScreen from '../../../views/Posting/Form/PostingFormScreen';

export default class PostingForm extends Component {
    constructor (props) {
        
        super (props);
    
    }

    render () {

        return <PostingFormScreen navigation={this.props.navigation} />

    }
}
