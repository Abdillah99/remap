/**
 * pembuat          : Mochammad Cusnul Abdillah 
 * tanggal mulai    : 24/04/2018
 * deskripsi        : Posting card
 * 
 */
import React, {Component} from 'react';
import {View, Text} from 'react-native';

//Views
import PostingScreen from "../../views/Posting/PostingScreen";

export default class Posting extends Component {

    constructor (props) {

        super (props);

    }

    render () {
  
        return  <PostingScreen navigation={this.props.navigation} />
  }
}
