import React, {Component} from 'react';
import { DrawerNavigator, StackNavigator, TabNavigator } from 'react-navigation';
import { Text } from 'react-native';

//import Controller disini ges

//Posting
import Posting from "../controllers/Posting/Posting";

//PostingForm
import PostingForm from "../controllers/Posting/Form/PostingForm";
import PostingFormPertanyaan from "../controllers/Posting/Form/PostingFormPertanyaan";
import PostingFormScreenPreview from "../controllers/Posting/Form/PostingFormPreview";

import Header from "../views/components/header";

/**
 * 
 * Posting App Stack
 * 
 */

 export const PostingStack = StackNavigator({

     Posting:{
        screen: Posting
    },
    
    PostingForm:{
        screen: PostingForm
    },
    
    PostingFormPertanyaan:{
        screen: PostingFormPertanyaan
    },
    
    PostingFormScreenPreview:{
        screen: PostingFormScreenPreview
    },
 },
 { 
    navigationOptions:{
        header: <Header/>
    }
 
});