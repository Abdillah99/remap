import React, { Component } from 'react';
import {  
    View, 
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
 } from 'react-native';

export default class ModalMenuPertanyaan extends Component {
    
    constructor(props){
        super(props);
        
        this.state = {
        
            menuSelected:this.props.menuID,
        
        }    
    }

    selectMenu = (menuID) => {
        this.setState({menuSelected:menuID});
        this.props.menuSelected(menuID);
    }
    
  render() {
  
    return (
        <View style={styles.menuContainer}>
        
            <View style={styles.item}>
                
                <TouchableWithoutFeedback onPress={() => this.selectMenu(1)}>
                
                    <View style={styles.button}>

                        <Image style={(this.state.menuSelected == 1)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/jawabanSingkat.png')} />
                                
                        <Text style={(this.state.menuSelected == 1)? styles.textSelected:styles.text}>Jawaban Singkat</Text>
    
                    </View>
                
                </TouchableWithoutFeedback>
                
                <TouchableWithoutFeedback onPress={() => this.selectMenu(2)}>

                    <View style={styles.button}>
                        
                        <Image style={(this.state.menuSelected == 2)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/pilihanGanda.png')} />

                        <Text style={(this.state.menuSelected == 2)? styles.textSelected:styles.text}>Pilihan ganda</Text>
                    
                    </View>

                </TouchableWithoutFeedback>
                
                <TouchableWithoutFeedback onPress={() => this.selectMenu(3)}>

                    <View style={styles.button}>
                        
                        <Image style={(this.state.menuSelected == 3)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/centang.png')} />

                        <Text style={(this.state.menuSelected == 3)? styles.textSelected:styles.text}>Centang</Text>
                    

                    </View>
                
                </TouchableWithoutFeedback>
                
                <TouchableWithoutFeedback onPress={() => this.selectMenu(4)}>
                
                    <View style={styles.button}>
                        
                        <Image style={(this.state.menuSelected == 4)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/event.png')} />

                        <Text style={(this.state.menuSelected == 4)? styles.textSelected:styles.text}>Tanggal</Text>
                    
                    </View>
                
                </TouchableWithoutFeedback>
                

            </View>

            <View style={styles.item}>
            
                <TouchableWithoutFeedback onPress={() => this.selectMenu(5)}>
                
                    <View style={styles.button}>

                        <Image style={(this.state.menuSelected == 5)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/paragraf.png')} />

                        <Text style={(this.state.menuSelected == 5)? styles.textSelected:styles.text}>Paragraf</Text>                
                    
                    </View>
                
                </TouchableWithoutFeedback>
            
                <TouchableWithoutFeedback onPress={() => this.selectMenu(6)}>
                    
                    <View style={styles.button}>
                        
                        <Image style={(this.state.menuSelected == 6)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/dropDownmenu.png')} />

                        <Text style={(this.state.menuSelected == 6)? styles.textSelected:styles.text}>Dropdown</Text>
                    
                    </View>
                
                </TouchableWithoutFeedback>
                
                <TouchableWithoutFeedback onPress={() => this.selectMenu(7)}>
                
                    <View style={styles.button}>
                        
                        <Image style={(this.state.menuSelected == 7)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/upload.png')} />

                        <Text style={(this.state.menuSelected == 7)? styles.textSelected:styles.text}>File Unggah</Text>
                    
                    </View>
                
                </TouchableWithoutFeedback>
                
                <TouchableWithoutFeedback onPress={() => this.selectMenu(8)}>
                    
                    <View style={styles.button}>
                        
                        <Image style={(this.state.menuSelected == 8)? styles.iconSelected:styles.icon}
                        source={require('../../assets/images/waktuKadaluarsa.png')} />

                        <Text style={(this.state.menuSelected == 8)? styles.textSelected:styles.text}>waktu</Text>
                    
                    </View>
                
                </TouchableWithoutFeedback>
            
            </View>
        
        </View>
        
        
    );
  }
}

const styles = StyleSheet.create({
    menuContainer:{
        elevation:5,
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    item:{
        flex:1,
        flexDirection: 'column',
        alignItems:'center',
    },
    
    button:{
        flexDirection:'column',
        alignItems:'center',
        margin: 6,
    },

    icon:{
        width:20,
        height:20,
    },

    text:{
        fontSize:10
    },

    iconSelected:{
        tintColor:'#29ba9b',
        width:20, 
        height:20
    },

    textSelected:{
        color:'#29ba9b',
        fontSize:10,
    },

})
