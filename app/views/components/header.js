/**
 * pembuat          : Mochammad Cusnul Abdillah 
 * tanggal mulai    : 23/04/2018
 * deskripsi        : Komponen Header
 * 
 */

import React, {Component} from 'react';
import {
    View, 
    Text, 
    StyleSheet, 
    TouchableOpacity,
    Image, 
    TextInput,
    Dimensions
} from 'react-native';


export default class Header extends Component {
    constructor(props){
        
        super(props);

    }

  render () {
    return (
      <View style={styles.container}>

        <TouchableOpacity style={styles.buttonBackContainer}>
            
            <View style={styles.buttonBackContainer}>
                
                <Image style={styles.buttonBackIcon} 
                source={require('../../assets/images/backArrow.png')}/>            

            </View>
        
        </TouchableOpacity>
        
        <View style={styles.logoContainer}>

            <Text style={styles.logo}>
            
            lounge
            
            </Text>

        </View>
        
        <View style={styles.buttonAl}>
        
            <Image style={styles.al} 
            source={require ('../../assets/images/al.png')} />
        
        </View>

      </View>
    );
  }
}

const dimensions = Dimensions.get ('window');

const styles = StyleSheet.create ({
  container: {
    alignItems: 'center',
    backgroundColor: '#f7f7ff',
    width: dimensions.width,
    height: Math.round (dimensions.height * 1 / 10),
    flexDirection: 'row',
    elevation: 4,
    shadowOpacity: 10,
  },

  buttonBackContainer: {
    flex:1,
    justifyContent: 'center',
    paddingLeft: 5,
  },

  buttonBackIcon:{
    width:20,
    height:20,
  },

  logoContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },

  logo:{
      fontFamily: 'FORCED SQUARE',
      fontSize: 30,
  },

  buttonAl: {
    flex: 1,
  },

  al:{
      width:24,
      height:24,
  },


});
