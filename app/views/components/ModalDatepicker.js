import React, { Component } from 'react';
import {  
    View, 
    Text,
    TouchableOpacity,
    StyleSheet, 
} from 'react-native';

import { Picker, DatePicker } from 'react-native-wheel-datepicker';

export default class ModalDatepicker extends Component {
    constructor(props){
        
        super(props);

        this.state = {
            firstDate: new Date(),
            secondDate: new Date(),
            thirdDate: new Date(),
            
        }
            
    }

    _handlePress = () =>{
        this.props.setState({isModalVisible: false});
    }

    render() {
    
        return (
        
        <View style={styles.container}>

            <View style={{flexDirection:'row', padding:10}}>

                <View style={{flex:1}}>

                    <Text style={{fontFamily:'Poppin-Regular', fontSize:16, alignSelf:'center'}}>Pilih Tanggal</Text>

                </View>
                
            </View>

            <View style={{flexDirection:'row'}}>
            
                <View style={{flex:1, backgroundColor:'#f7f7ff', padding:20,}}>

                    <Text style={{fontFamily:'Poppin-Bold', fontSize:16, alignSelf:'center'}}>{this.state.date && this.state.date.toJSON()}</Text>

                </View>

            </View>
           
                <DatePicker 
                    mode="date"
                    textSize={20}
                    textColor='black'
                    onDateChange={date => this.setState({date})}
                    style={{backgroundColor:'#f7f7ff'}}/>

                <View style={styles.buttonContainer}>

                    <TouchableOpacity onPress={() => this.props.modalHandlePress('batal')}
                    style={[styles.buttonStyle, {backgroundColor:'#848285', padding:10}]}>

                        <Text style={{alignSelf:'center', color:'white'}}>BATAL</Text>

                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.buttonStyle, {backgroundColor:'#f79220', padding:10}]}>

                        <Text style={{alignSelf:'center', color:'white'}}>Lanjutkan</Text>

                    </TouchableOpacity>
                
                </View>

        </View>
        
        );
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#fff',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20,
    },

    buttonContainer:{
        flexDirection:'row',
        padding:10,
    },

    buttonStyle:{
        flex:1,
        borderRadius: 50,

    },
    

});