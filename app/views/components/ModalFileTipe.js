import React, { Component } from 'react';
import {  
    View, 
    Text, 
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

import CheckBox from 'react-native-check-box';

export default class ModalFileTipe extends Component {
    
    constructor(props){
        super(props);
        
        this.state={
            filetipe:{

                dokumen:this.props.dokumenChecked,
                gambar:this.props.gambarChecked,
                video:this.props.videoChecked,
                audio:this.props.audioChecked,

            },

        }

    }

    sendData = () =>{
        
        this.props.receiveData(this.state.filetipe);

    }
    
    checkBoxChecked = (id) =>{

        if(id == 'dokumen'){
            
            this.setState({
                filetipe:{

                    dokumen: !this.state.filetipe.dokumen,
                    gambar:this.state.filetipe.gambar,
                    video:this.state.filetipe.video,
                    audio:this.state.filetipe.audio,

                }
            });

        }else if(id == 'gambar'){

            this.setState({
                filetipe:{

                    dokumen: this.state.filetipe.dokumen,
                    gambar: !this.state.filetipe.gambar,
                    video: this.state.filetipe.video,
                    audio: this.state.filetipe.audio

                }
            });

        }else if(id == 'video'){
            this.setState({
                filetipe:{
                    
                    dokumen: this.state.filetipe.dokumen,
                    gambar: this.state.filetipe.gambar,
                    video: !this.state.filetipe.video,
                    audio: this.state.filetipe.audio
                
                }
            });

        }else if(id == 'audio'){

            this.setState({
                filetipe:{

                    dokumen: this.state.filetipe.dokumen,
                    gambar: this.state.filetipe.gambar,
                    video: this.state.filetipe.video,
                    audio: !this.state.filetipe.audio
                
                }
            });

        }else{

            alert('checkBox error because null state');
        
        }
        
    }
    
  render() {
    return (
    <View style={styles.modalContainer}>

        <View style={styles.checkBoxContainer}>
            
            <CheckBox
                style={styles.checkBoxstyle}
                rightText={'Dokumen'}
                isChecked={this.state.filetipe.dokumen}
                onClick={()=>this.checkBoxChecked('dokumen')}
            />
        
            <CheckBox
                    rightText={'Video'}
                    style={styles.checkBoxstyle}
                    isChecked={this.state.filetipe.video}
                    onClick={() => this.checkBoxChecked('video')}
            /> 

         </View> 
        

        <View style={styles.checkBoxContainer}>
        
            <CheckBox
                style={styles.checkBoxstyle}
                rightText={'Gambar'}
                isChecked={this.state.filetipe.gambar}
                onClick={()=>this.checkBoxChecked('gambar')}
            />

        
            <CheckBox
                rightText={'Audio'}
                style={styles.checkBoxstyle}
                isChecked={this.state.filetipe.audio}
                onClick={() => this.checkBoxChecked('audio')}
            />  
        
        </View>

        <View style={styles.buttonContainer}>

            <View style={{flex:3}}/> 

            <View style={{flex:7, flexDirection:'row', padding:4}}>
                
                <TouchableOpacity 
                style={styles.buttonStyle}>

                    <Text>Batal</Text>
                
                </TouchableOpacity>

                <TouchableOpacity onPress={this.sendData}
                style={[styles.buttonStyle, {backgroundColor:'#29ba9b'}]}>
                
                    <Text>OK</Text>
                
                </TouchableOpacity>

            </View>

        </View>
        

    </View>
    );
  }
}

const styles = StyleSheet.create({
    modalContainer:{
        flex:1,
        flexDirection: 'column',
    },

    checkBoxContainer:{
        flex:1,
        flexDirection:'row',
    },

    checkBoxstyle:{
        flex: 1,
    },

    buttonContainer:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'flex-end',
    },

    buttonStyle:{
        flex:1,
        borderRadius: 50,
        justifyContent:'center',
        alignItems: 'center',
    }
})
