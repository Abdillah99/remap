import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';

export default class ModalPilihGambar extends Component {
    constructor(props){
        
        super(props);

        this.state = {
            imageData: null
        }
            
    }


    _openCamera = () => {
        var options = {
        
        };
        
      ImagePicker.launchCamera(options, (response)  => {
      
        console.log('Response = ', response);
			
			if(response.didCancel){
      
        console.log('Cancelled');
      
      }else if (response.error){
      
        console.log('error ' , response.error);
      
      }else {
      
        let source = response.uri;
				this.setState({imageData: source});
				this.props.receiveData(this.state.imageData);
			}

        });
    
    }

  _openGallery = () => {
    var options = {

    };
      ImagePicker.launchImageLibrary(options, (response)  => {
        console.log('Response = ', response);
		
		if(response.didCancel){
			console.log('Cancelled');
		}else if (response.error){
			console.log('error ' , response.error);
		}else {
			let source = response.uri;
			this.setState({imageData: source});
			this.props.receiveData(this.state.imageData);
		}
		
      });

  }

    render () {
        return (
            
            <View style={styles.modalContainer}>

                <View style={styles.modalHeader}>
         
                    <Text>Pilih Sumber Media dari</Text>
                
                </View>

                <View style={styles.modal}>

                    <View style={styles.modalButton}>
           
                         <TouchableOpacity onPress={this._openCamera}>
                        
                            <Image style={styles.modalIcon}
                            source={require ('../../assets/images/openCamera.png')}/>

                        </TouchableOpacity>
                    
                        <Text>Kamera</Text>
                    
                    </View>

                    <View style={styles.modalButton}>
                    
                    <TouchableOpacity onPress={this._openGallery}>
                    
                    <Image style={styles.modalIcon}
                    source={require ('../../assets/images/openGallery.png')}/>

                    </TouchableOpacity>
            
                    <Text>Galeri</Text>
                    
                    </View>

                </View>

             </View>
    );
  }
}

const styles = StyleSheet.create ({
  modalContainer: {
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },

  modalHeader: {
    justifyContent: 'center',
    alignContent: 'center',
  },

  modal: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },

  modalButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },

  modalIcon: {
    width: 40,
    height: 40,
  },
});
