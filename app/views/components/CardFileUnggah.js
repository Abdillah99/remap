import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';

import CheckBox from 'react-native-check-box';

import Modal from 'react-native-simple-modal';

import ModalFileTipe from '../components/ModalFileTipe';


export default class CardFileUnggah extends Component {
    constructor(props){
        
        super(props);

        this.state={

            fileTipe:{
                dokumen:this.props.dokumenChecked,
                gambar:this.props.gambarChecked,
                video:this.props.videoChecked,
                audio:this.props.audioChecked,
            },

            filetipeText:[],

        }
    }
    
    renderText(){
        if(this.props.dokumenChecked ){
            this.state.filetipeText.push('Dokumen')
        }else if(this.props.gambarChecked){
            this.state.filetipeText.push('Gambar')
        }else if(this.props.audioChecked){
            this.state.filetipeText.push('Audio')
        }else if (this.props.videoChecked){
            this.state.filetipeText.push('Video')
        }
    }


    render() {
        return(
            <View style={styles.container}>

                <View style={styles.buttonContainer}>

                    <TouchableWithoutFeedback onPress={this.props.openModal}>
                    
                        <View style={styles.button}>

                            <Image source={require('../../assets/images/upfile.png')}
                            style={styles.image}/>

                        </View>
                    
                    </TouchableWithoutFeedback>

                        <Text style={fontHelper.PoppinLight}>Tipe File</Text>
                                                       
                        <Text numberOfLines={4}
                        style={fontHelper.PoppinBold}>
                                
                        {this.props.audioChecked ? 'Audio' : this.props.dokumenChecked ? 'Dokumen' : this.props.videoChecked ? 'Video': this.props.gambarChecked ? 'Gambar': null}
                        </Text>
                </View>
    
                <View style={styles.buttonContainer}> 

                    <View style={styles.button}>

                        <Image source={require('../../assets/images/copy.png')}
                        style={styles.image}/>

                    </View>

                    
                    <Text style={fontHelper.PoppinLight}>Max.Jumlah</Text>
                    <Text style={fontHelper.PoppinBold}>1K</Text>

                </View>

                <View style={styles.buttonContainer}> 

                    <View style={styles.button}>

                        <Image source={require('../../assets/images/upfile.png')}
                        style={styles.image}/>

                    </View>

                     <Text style={fontHelper.PoppinLight}>Max.Ukuran</Text>
                     <Text style={fontHelper.PoppinBold}>1TB</Text>
                </View>

            </View>
        )
    }    
}

const fontHelper = (require('../../assets/styles/fontHelper'));

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection: 'row',
        marginTop: 25,
        marginBottom: 25,
    },
    
    buttonContainer:{
        flex:1,
        flexDirection:'column',
        justifyContent: 'flex-start',
        alignItems: 'center',

    },

    image:{
        width:15,
        height:15,
    },

    button:{
        width:45,
        height:45,
        marginBottom:10,
        justifyContent: 'center',
        alignItems:'center',
        borderRadius: 100,
        borderColor: '#848285',
        borderWidth: 1,
    },

});