import React, { Component } from 'react';
import {  
    View, 
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,

} from 'react-native';

import Modal from 'react-native-simple-modal';

import {RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import ModalDropdown from 'react-native-modal-dropdown';

import ModalMenuPertanyaan from '../components/ModalMenuPertanyaan';

const fontHelper = (require('../../assets/styles/fontHelper'));

export default class CardPertanyaan extends Component {

    constructor(props){
	
		super(props);
	
		this.state = {
			
			text:'',
	
			open:false,
	
			offset:0,	
	
		}    
	}
	
	moveUp = () => this.setState({offset: -100})

	modalDidOpen = () => console.log('modal openend')

	modalDidClose = () => console.log('modal closed')
	
	resetPosition = () => this.setState({offset: 0})

	openModal = () => this.setState({open: true})
  
	closeModal = () => this.setState({open: false})


    onSelect(index, value){
		
	}

	
  render() {
    return (
    
    <View style={styles.container1}>
	
		<View style={styles.container1Header}>
		
			<ModalDropdown 
                dropdownStyle={{height:100, flexDirection:'column'}}
                options={[
                
                    <Image style={{width:30, height:30}}
                    source={require('../../assets/images/share.png')}/>, 
                    'tes',  
                ]}>

                <Image style={styles.buttonOptions}
                source={require ('../../assets/images/options.png')}/>
            
            </ModalDropdown>
            
        
        </View>

        <View style={styles.container1Content}>
            
            <TextInput placeholder="Tulis Pertanyaan"/>

            <View style={styles.container1ContentButton}>

                <View style={styles.realButtonContainer}>
					<TouchableOpacity onPress={this.openModal}
					style={styles.buttonStyle} >
						
						<Image style={styles.buttonIconStyle} 
                        source={require('../../assets/images/pilihanGanda.png')}/>
						
							<Text style={styles.buttonTextStyle}>Pilihan Ganda</Text>
						
					</TouchableOpacity>

                    <RadioGroup 
                    color='#7d7d7d7d'
                    onSelect = {(index, value) => this.onSelect(index, value)}>

                        <RadioButton value={'item1'} >

						    <Text style={fontHelper.PoppinRegular} >Opsi 1</Text>

                        </RadioButton>

                        <RadioButton value={'item2'}>

                            <Text style={fontHelper.PoppinRegular}>Tambah Opsi</Text>

                        </RadioButton>


                    </RadioGroup>

                </View>

                <View style={styles.empty}/>
            
            </View>

        </View>

        <View style={{flexDirection:'row', justifyContent:'flex-end'}}>
            
            <RadioGroup 
            color='#7d7d7d7d'
            onSelect = {(index, value) => this.onSelect(index, value)}>
            
                <RadioButton value={'item1'} >
                
                    <Text>Wajib diisi</Text>
                
                </RadioButton>


            </RadioGroup>

        </View>

		<Modal
			offset={this.state.offset}
			open={this.state.open}
			modalDidOpen={this.modalDidOpen}
			modalDidClose={this.modalDidClose}
			overlayBackground="transparent"
			closeOnTouchOutside={true}
			modalStyle={{
				width:200,
				borderRadius:5, 
				backgroundColor:'white', 
				elevation:5,
				}}>

			<ModalMenuPertanyaan/>

		</Modal>

    
    </View>

    );
  }
}

const styles = StyleSheet.create ({
		container1:{
			borderWidth: 0.1,
			borderRadius: 10,
			flexDirection: 'column',
			borderColor: '#848285',
            margin:10,
            elevation:1,
		},

		container1Header:{
			flexDirection:'row',
			justifyContent: 'flex-end',
		},

		buttonOptions: {
			width: 20,
			height: 20,
			margin: 10,
		  },
		
		container1Content:{
			flex:9.8,
			flexDirection: 'column',
			marginLeft:16,
			marginRight:16,
		},

		container1ContentButton:{
			flexDirection:'row',
			borderBottomColor: '#7d7d7d',
			borderBottomWidth: 0.3,
		},

		realButtonContainer:{
			flex:1,
		},

		empty:{
			flex:1,
		},

		buttonStyle:{
			borderRadius:50,
			alignContent: 'center',
			backgroundColor: '#29ba9b',	
			flexDirection:'row',
			padding: 5,
		},

		buttonIconStyle:{
			width: 16,
			height: 16,
			alignSelf: 'center',
			tintColor: 'white',
			marginLeft:5,
		},

		buttonTextStyle:{
			fontFamily: 'PoppinBold',
			fontSize: 16,
			color:'#fff',
			marginLeft:6,
		},

		container2:{
			flex:0.8,
			marginLeft: 5,
			marginRight: 5,
		},

		buttonTambah:{
			flex:1,
			flexDirection:'row',
			justifyContent: 'center',
			alignItems: 'center',
			borderRadius: 10,
			borderWidth:0.5,
			marginTop: 12,
			marginLeft:15,
			marginRight:15,
			borderColor: '#848285',
		},

		emptyContainer:{
			flex:3,
		},

		container3:{
			flex:2,
			marginLeft:10,
			marginRight:10,
			flexDirection:'column',
			justifyContent: 'flex-end',
		},

		buttonPreview:{
			borderRadius: 50,
			borderWidth:0.5,
			justifyContent: 'center',
			alignItems:'center',
			borderColor: '#848285',
			marginBottom:4,
		},

		buttonFooterStyle:{
			flex:1,
			padding:6,
			justifyContent: 'center',
			alignItems:'center',
			borderRadius:50,
			marginTop:10,
			marginBottom:10,
		},
		
		textFooter:{
			fontSize:16,
			color: 'white',
			fontFamily: 'PoppinBold',
		},

		viewHolder:{
			height: 55,
			backgroundColor: '#26A69A',
			justifyContent: 'center',
			alignItems: 'center',
			margin: 4
		},

		text:{
			color: 'white',
			fontSize: 25
		},

  });

