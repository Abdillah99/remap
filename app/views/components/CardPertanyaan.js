import React, { Component } from 'react';
import {  
    View, 
    Text, 
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    Dimensions,
} from 'react-native';

import Modal from 'react-native-simple-modal';

import ModalDropdown from 'react-native-modal-dropdown';

import CheckBox from 'react-native-check-box';


import ModalMenuPertanyaan from '../components/ModalMenuPertanyaan';
import PilihanGanda from '../components/PilihanGanda';

import CardFileUnggah from '../components/CardFileUnggah';
import JawabanSingkat from './JawabanSingkat';
import ModalFileTipe from '../components/ModalFileTipe';

const dimensions = Dimensions.get('window');

const fontHelper = require('../../assets/styles/fontHelper');

export default class sCardPertanyaan extends Component {
    constructor(props){
        
        super(props);

        this.state ={

            modalOpened:false,

            offset:0,

            wajib:false,

            menuID:2,
            
            modalID:null,

            fileTipe:{

                dokumen:false,

                gambar:false,

                video:false,

                audio:false,

            }

        }
         
    }

	openModal = (id) => {

        this.setState({modalID: id});
        
        this.setState({modalOpened: !this.state.modalOpened});

    }

    menuSelected = ( menuID ) =>{
        
        this.setState({menuID: menuID});
    
        this.setState({modalOpened: !this.state.modalOpened});
    
    }

    fileTipe = ( data ) => {

        this.setState({fileTipe: data});
        this.setState({modalOpened: false});

    }

    setOffset = (position) => this.setState({offset: position})

    modalDidClose = () => this.setState({modalOpened:false})

    checkBox = () => this.setState({wajib:!this.state.wajib})
    
    render() {

    return (
        <View style={styles.cardContainer}>

            <View style={styles.headerContainer}>

                <Image style={{width:20, height:20, margin:10}}
                source={require('../../assets/images/options.png')}/>
        
            </View>

            <View style={styles.contentContainer}>
                    
                    <View style={{flexDirection:'column', justifyContent:'flex-start'}}>
                    
                        <TextInput placeholder="Tulis Pertanyaan"/>
                        
                        <View style={{flexDirection:'row'}}>

                                {this.state.menuID == 1 && 
                                
                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >
                                    
                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/jawabanSingkat.png')}/>

                                    <Text style={styles.buttonTextStyle}>JawabanSingkat</Text>

                                </TouchableOpacity>
                                
                                }

                                {this.state.menuID == 2 && 

                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/pilihanGanda.png')}/>

                                    <Text style={styles.buttonTextStyle}>Pilihan Ganda</Text>

                                </TouchableOpacity>

                                }
                                
                                {this.state.menuID == 3 && 

                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/centang.png')}/> 

                                    <Text style={styles.buttonTextStyle}>Centang</Text>

                                </TouchableOpacity>

                                }

                                {this.state.menuID == 4 && 

                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/event.png')}/>

                                    <Text style={styles.buttonTextStyle}>Tanggal</Text>

                                </TouchableOpacity>

                                }
                                {this.state.menuID == 5 && 

                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/paragraf.png')}/>

                                    <Text style={styles.buttonTextStyle}>Paragraf</Text> 

                                </TouchableOpacity>

                                }

                                {this.state.menuID == 6 && 

                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/dropDownmenu.png')}/>

                                    <Text style={styles.buttonTextStyle}>Dropdown</Text>

                                </TouchableOpacity>

                                }
                                
                                {this.state.menuID == 7 && 

                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/upload.png')}/> 

                                    <Text style={styles.buttonTextStyle}>File Unggah</Text>

                                </TouchableOpacity>
                                
                                }

                                {this.state.menuID == 8 && 
                                
                                <TouchableOpacity onPress={() => this.openModal('menu')}
                                style={styles.buttonStyle} >

                                    <Image style={styles.buttonIconStyle}
                                    source={require('../../assets/images/waktuKadaluarsa.png')}/> 

                                    <Text style={styles.buttonTextStyle}>Waktu</Text>

                                </TouchableOpacity>
                                
                                }
                            
                            <View style={{flex:0.5}}></View>
                        
                        </View>

                    </View>
            
            </View>

            <View style={styles.pertanyaanContainer}>
                    
                    {this.state.menuID == 1 && <JawabanSingkat/>}
                    {this.state.menuID == 2 && <PilihanGanda/>}
                    {this.state.menuID == 3 && <View><Text>kosong</Text></View>}
                    {this.state.menuID == 4 && <View><Text>kosong</Text></View>}
                    {this.state.menuID == 5 && <View><Text>kosong</Text></View>}
                    {this.state.menuID == 6 && <View><Text>kosong</Text></View>}
                    {this.state.menuID == 7 && 
                        <CardFileUnggah 
                            openModal={() => this.openModal('filetipe')}
                            dokumenChecked={this.state.fileTipe.dokumen}
                            gambarChecked={this.state.fileTipe.gambar}
                            audioChecked={this.state.fileTipe.audio}
                            videoChecked={this.state.fileTipe.video}
                            />
                    }

                    {this.state.menuID == 8 && <View><Text>kosong</Text></View>}

            </View>

            <View style={styles.footerContainer}>
                
                <View style={{flex:1}}/>
                
                <CheckBox
                    style={{flex:1,flexDirection:'row', justifyContent:'flex-end', marginRight:3}}
                    onClick={() => this.setState({wajib:!this.state.wajib})}
                    isChecked={this.state.wajib}
                    checkedImage={<Image source={require('../../assets/images/pilihanGanda.png')} style={{justifyContent:'center', width:20, height:20, tintColor:'#f79220'}}/>}
                    unCheckedImage={<Image source={require('../../assets/images/radioButtonUnselected.png')} style={{justifyContent:'center', width:20, height:20}}/>}
                />

                <Text style={[{alignSelf:'center'}, fontHelper.PoppinRegular]}>Wajib diisi</Text>

            </View>

            <Modal 
                offset={(this.state.modalID == 'menu' ? 0 : 50)}
                open={this.state.modalOpened}
                modalDidClose={this.modalDidClose}
                overlayBackground="transparent"
                closeOnTouchOutside={true}
                modalStyle={ (this.state.modalID == 'menu') ? styles.modalStyleMenu : styles.modalStyleFitipe } >

                {this.state.modalID == 'filetipe' && 
                    
                    <ModalFileTipe receiveData={this.fileTipe}
                        dokumenChecked={this.state.fileTipe.dokumen}
                        gambarChecked={this.state.fileTipe.gambar}
                        audioChecked={this.state.fileTipe.audio}
                        videoChecked={this.state.fileTipe.video}
                    /> 
                
                }
                
                {this.state.modalID == 'menu' && 
                    
                    <ModalMenuPertanyaan 
                        menuID={this.state.menuID} 
                        menuSelected={this.menuSelected}
                    />
                
                }



		    </Modal>
        
        </View>


    );
  }
}

const styles = StyleSheet.create({
    modalStyleMenu:{
        width:200,
        borderRadius:5, 
        backgroundColor:'white', 
        elevation:5,
    },

    modalStyleFitipe:{
        width:250,
        height: 150,
        borderRadius:5,
        backgroundColor:'white',
        elevation:5,
    },

    cardContainer:{
        flex:1,
        borderWidth: 0.1,
        borderRadius: 10,
        margin:8,
        flexDirection: 'column',
        justifyContent:'flex-start',
        elevation:1,
        padding:10,
    },

    headerContainer:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'flex-end',
        // borderWidth:1,
    },

    contentContainer:{
        flex:5.5,
        flexDirection: 'column',
        // borderWidth:1,
    },

    buttonStyle:{
        borderRadius:50,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#29ba9b',	
        flexDirection:'row',
        paddingBottom: 5,
        paddingTop: 5,
        paddingRight: 10,
        paddingLeft: 10,
    },

    buttonIconStyle:{
        width:16, 
        height:16, 
        alignSelf:'center', 
        tintColor:'white',
        marginLeft:5
    },

    buttonTextStyle:{
        fontSize:16,
        alignItems: 'center',
        color:'white', 
        marginLeft:6
    },

    pertanyaanContainer:{
        flex:4.5,
        flexDirection:'column',
        borderBottomColor: '#848285',
        borderBottomWidth: 0.4,
    },

    footerContainer:{
        flex:1.5,
        flexDirection:'row',
        justifyContent: 'flex-end',
        padding:5,
        // borderWidth:1,
    },
})
