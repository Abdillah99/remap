import React, { Component } from 'react';
import {  
	View, 
	Text, 
	StyleSheet, 
	TextInput, 
	Dimensions 
} from 'react-native';

export default class JawabanSingkat extends Component {
  render() {
    return (
      <View style={styles.cardContainer}>

			<TextInput placeholder="Deskripsi"/>

			<TextInput placeholder="Jawaban singkat"/>
		
		</View>


    );
  }
}
const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({

    cardContainer:{
		flexDirection: 'column',
		margin: 10,
		paddingBottom: 20,
	},

	textInputContainer:{
		flexDirection:'row'
	},

    textInput:{
        flex:1,
    },

}) 
