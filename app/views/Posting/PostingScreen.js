import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import { MenuProvider, Menu, MenuTrigger, MenuOption, MenuOptions } from 'react-native-popup-menu';

export default class PostingScreen extends Component {
    constructor (props) {
        super (props);
        this.state = {
        postingText: null,
    };
}

render () {
    const {navigate} = this.props.navigation;

    return <View style={styles.container}>
    
    <View style={styles.postingCard}>
    
        <View style={styles.postingHeader}>
        
            <View style={styles.userPhotoContainer}>
            
                <Image style={styles.userPhoto}
                source={require ('../../assets/images/user.png')}/>

            </View>

          <View style={styles.userNameContainer}>

            <Text style={[styles.PoppinSemiBold, {fontSize: 16}]}>
              Joung Alexander
            </Text>

            <Text style={[styles.PoppinLight, {fontSize: 10}]}>
            English
            </Text>

          </View>

          <View style={styles.buttonOptions}>
                
				<Image style={styles.buttonIcon}
            	source={require ('../../assets/images/options.png')}/>
            
          </View>

        </View>

        <View style={styles.postingContainer}>

          <TextInput
            style={styles.textInputStyle}
            placeholder="Tulis Post"
            underlineColorAndroid="transparent"
            multiline={true}/>

        </View>

        <View style={styles.postingFooter}>

          <Image
            style={styles.buttonIcon}
            source={require ('../../assets/images/addcam.png')}/>

          <Image
            style={styles.buttonIcon}
            source={require ('../../assets/images/sticker.png')}/>

          <Image
            style={styles.buttonIcon}
            source={require ('../../assets/images/pinmaps.png')}/>

          <Image
            style={styles.buttonIcon}
            source={require ('../../assets/images/event.png')}/>

          <TouchableOpacity onPress={() => navigate ('PostingForm')}>

            <Image
              style={styles.buttonIcon}
              source={require ('../../assets/images/forms.png')}/>

          </TouchableOpacity>

          <Image
            style={styles.buttonIcon}
            source={require ('../../assets/images/share.png')}/>

          <Image
            style={styles.buttonIcon}
            source={require ('../../assets/images/send.png')}/>

        </View>

      </View>

    </View>;
  }
}

const globalStyles = require ('../../assets/styles/global');
const styles = Object.assign (
  {},
  globalStyles,
  StyleSheet.create ({
    postingCard: {
      flex: 1,
      margin: 10,
      backgroundColor: '#fff',
      borderColor: '#848285',
      borderWidth: 0.5,
      borderRadius: 16,
      flexDirection: 'column',
      justifyContent: 'space-between',
      elevation: 2,
    },

    postingHeader: {
      flex: 1.5,
      flexDirection: 'row',
      borderBottomColor: '#848285',
      borderBottomWidth: 0.5,
      justifyContent: 'flex-start',
      alignItems: 'center',
    },

    userPhotoContainer: {
      flex: 1.5,
      justifyContent: 'center',
      alignItems: 'center',
    },

    userPhoto: {
      width: 30,
      height: 30,
      resizeMode: 'cover',
      alignSelf: 'center',
      borderWidth: 0.2,
      borderColor: '#000',
      borderRadius: 100,
    },

    userNameContainer: {
      flex: 7,
      flexDirection: 'column',
      justifyContent: 'space-between',
    },

    buttonOptions: {
      flex: 1.5,
    },

    buttonOptionsIcon: {
      width: 5,
      height: 5,
    },

    postingContainer: {
      flex: 7,
    },

    textInputStyle: {
      margin: 10,
      fontFamily: 'Poppins-Regular',
    },

    postingFooter: {
      flex: 1.5,
      flexDirection: 'row',
      borderTopColor: '#848285',
      borderTopWidth: 0.5,
    },

    buttonIcon: {
      width: 20,
      height: 20,
      margin: 10,
    },
  })
);
