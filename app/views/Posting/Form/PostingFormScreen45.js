import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  Image,
} from 'react-native';

export default class Respon extends Component{

static navigationOptions = {
    headerStyle: {
      backgroundColor: "#16a085",
      elevation: null,
      title : 'LOUNGE',
    },
};

render() {
 	return (
 		<View style={styles.Utama}>
	  		<View style={styles.MainContainer}>
	  			<View style={styles.responden}>
	  				<Image source={require('../../../assets/images/ainsley-harriott-celebrity-mask.jpg')} style={styles.icon}/>
	  					<View style={styles.textprof}>
	  						<View style={styles.profil}>
	  							<Image source={require('../../../assets/images/lounge_ic_jam.png')} style={styles.iconjam}/>
	  							<Text style={styles.jam}> 24.59</Text>
	  							<Text> ● </Text>
	  							<Text style={styles.tangal}>22 Des 2017</Text>
	  						</View>
	  						<View style={styles.respon}>
	  							<Image source={require('../../../assets/images/rsz_file-gross-symbol.png')} style={styles.iconfolded}/>
	  							<Text style={styles.angka}>0</Text><Text style={styles.itungan}>Respon</Text>
	  						</View>
	  					</View>		
	  			</View>
	  			<View style={styles.nameform}>
	  				<Text style={styles.forms}> Form</Text>
	  				<Text style={styles.judul}> REG CONCERT TOUR 2018</Text>
	  				<Text style={styles.warn}> * Wajib Diisi</Text>
	  			</View>
	  		</View>
	  		<View style={styles.SecondContainer}>
					<View style={styles.input}>
						<View style={styles.namaionas}>
						<Text style={styles.textnama}>Nama Anda</Text><Text style={styles.bintang}>*</Text>
						</View>
						<View style={styles.garis}/>
						<TextInput placeholder="Tuliskan Nama Lengkap Anda " underlineColorAndroid='black' style={styles.textnamaleng}/>
						<TextInput placeholder="Tulis Jawaban" underlineColorAndroid='black' style={styles.textjawab}/>
					</View>
					<View style={styles.unggah}>
						<View style={styles.namaionas}>
						<Text style={styles.textunggah}> Unggah Foto Anda</Text><Text style={styles.bintang}>*</Text>
						</View>
						<View style={styles.garis}></View>
						<TouchableOpacity style={styles.button}>
							<Image source={require('../../../assets/images/upload.png')} style={styles.iconupload}/>
      						<Text style={styles.btntext}>Unggah File </Text>
      					</TouchableOpacity>
					</View>		
			</View>
			<View style={styles.ThirdContainer}>
					<TouchableOpacity style={styles.buttonkirim}>
      					<Text style={styles.btntext1}>KIRIM RESPON</Text>
      				</TouchableOpacity>
			</View>					
		</View>	   
   )
  }
}

const styles = StyleSheet.create({

Utama: {
	flex: 1,
},
MainContainer :{
	justifyContent: 'center',
	flex: 0.4,
    backgroundColor: '#F7F7FF',
    borderWidth: 1,
    borderColor: '#E8E6E8',
},
SecondContainer :{
	flexDirection: 'column',
	paddingLeft: 20,
	paddingRight: 20,
	flex: 0.7,
	backgroundColor: '#fff',
	paddingTop: 20,
},
ThirdContainer :{
	paddingLeft: 20,
	paddingRight: 20,
	flex: 0.1,
	backgroundColor: '#fff',
	paddingTop: 120,
	marginBottom: 10,
},
formunggah:{
	height: 170,
	flex: 0.7,
	borderRadius: 10,
	borderWidth : 0.7,
	paddingLeft: 20,
	paddingRight : 20,
	paddingBottom: 50,
},
unggah:{
	marginTop: 15,
	height: 110,
	paddingLeft: 20,
	paddingRight : 20,
	borderRadius : 10,
	borderWidth: 0.7,
},
Form2:{
	flexDirection: 'column',
	justifyContent: 'center',
},
responden:{
	flexDirection: 'row',
	flex : 1,
	marginLeft: 10,
	paddingTop : 20,
},
input:{
	height: 170,
	paddingLeft: 20,
	paddingRight : 20,
	borderRadius : 10,
	borderWidth: 0.7,
},
namaionas:{
	flexDirection: 'row',
},
nameform :{
	flexDirection: 'column',
	paddingBottom : 5,
},
icon:{
	borderWidth : 10,
	paddingLeft : 10,
	width: 70,
	height: 70,
	borderRadius: 1000,
	borderWidth: 1,
	borderColor: '#000000',	
},
textnama:{
	paddingTop : 5,
	fontSize: 20,
	color: '#636363',
	fontWeight: 'bold',
	fontFamily: 'Poppins-Medium',
},
text :{
	textAlign: 'center',
},
textunggah: {
	paddingTop: 5,
	textAlign: 'left',
	fontSize: 20,
	color: '#636363',
	fontWeight: 'bold',
},
forms:{
	fontSize: 15,
	paddingLeft: 15,
	paddingTop: 60,
},
garis:{
	height: 1,
	backgroundColor: "#000000",
	opacity: 0.5,
},
iconfolded:{
	marginLeft: 20,
	marginTop: 10,
},
judul:{
	paddingLeft: 15,
	fontWeight: 'bold',
	fontSize : 22,
	color: '#3D3D3E',
},
bintang:{
	color: '#ECAF5B',
	fontSize : 20,
	marginLeft: 10,
},
warn:{
	paddingLeft: 15,
	color: '#ECAF5B',
	fontSize : 15,
},
textprof :{
	flexDirection: 'column',
},
profil: {
	flexDirection: 'row',
	marginTop: 10,
},
angka:{
	paddingTop : 10,
	flexDirection: 'column',
	paddingLeft : 15,
	color: '#54B9A4',
},
jam:{
	marginLeft: 10,
	textAlign:'center',
	flexDirection: 'column',
},
iconjam:{
	marginLeft: 20,
},
tanggal:{
	marginLeft: 50,
	textAlign: 'center',
},
respon:{
	flexDirection : 'row',
},
iconupload:{
	width: 25,
	height: 25,
	marginLeft: 10,
},
itungan: {
	paddingTop : 10,
	flexDirection: 'column',
	paddingLeft : 5,
},
btntext: {	
  marginLeft: 10,
  paddingBottom: 5,
  fontSize: 20,
  color : '#fff',
  fontWeight: 'bold',
  },
button: {
  flexDirection: 'row',
  marginTop: 20,
  borderRadius: 30,
  alignItems: 'center',
  backgroundColor: '#29BA9B',
  marginRight: 120,
  alignSelf: 'stretch',

},
buttonkirim:{
  paddingLeft: 50,
  paddingRight: 50,
  borderRadius: 30,
  alignItems: 'center',
  backgroundColor: '#BEBEC6',
  paddingTop : 10,
  alignSelf: 'stretch',
  marginBottom: 40,
},
btntext1: {	
  paddingBottom: 5,
  fontSize: 20,
  color : '#fff',
  fontWeight: 'bold',
  },
});