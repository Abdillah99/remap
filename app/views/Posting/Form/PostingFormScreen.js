import React, { Component } from 'react';
import {  
    View, 
    Text, 
    TextInput, 
    Image, 
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableOpacity,
 } from 'react-native';

import Modal from "react-native-modal";

import ModalPilihGambar from "../../components/ModalPilihGambar";
import ModalDatepicker from "../../components/ModalDatepicker";

export default class PostingFormScreen19 extends Component {
  
    constructor(props){
        
        super(props);
            this.state = {

                labelFoto: null,

                tanggal:null,

                hakAkses:null,
    
                verifikasi:false,

                acak:false,

                isModalVisible: false,

                modalID:null,

            };
    }

    receiveData = ( imageData ) => {
        this.setState({labelFoto: imageData});
        this.setState({isModalVisible:false});
    }

    _toggleModal = (id) =>{
        this.setState({isModalVisible: !this.state.isModalVisible});
        this.setState({modalID: id});
    }

    _modalHandlePress = (id) =>{
        if(id == 'batal'){
            this.setState({isModalVisible: false});
        }
    }

    buttonHandlepress = (id) => {
        
        if(id == 'verifikasi'){
        
            this.setState({verifikasi: !this.state.verifikasi});
        
        }else if(id == 'acak'){
        
            this.setState({acakPertanyaan: !this.state.acakPertanyaan});
        }

    }
        
	render() {
        const {navigate} = this.props.navigation;
		return <View style={styles.container}>
			<View style={styles.formHeader}>
			
				<TextInput style={styles.formHeaderTextInput} 
				placeholder="Judul Form"/>

			</View>

			<View style={styles.formContainer}>
				
				<View style={styles.buttonContainer}>

                    <View style={styles.circleButtonContainer}>

                        <TouchableWithoutFeedback onPress={() => this._toggleModal('camera')}>
                            {
                                this.state.labelFoto ? 
                                <View style={styles.circleButton}>
                                    <Image style={styles.labelImage}
                                    source={{uri: this.state.labelFoto}}
                                    resizeMode="cover"/>
                                </View>
                                
                                
                            :   <View style={styles.circleButton}>
                                    <Image style={styles.circleButtonIcon}
                                    source={require('../../../assets/images/picture.png')}/>
                                </View>
                            }
                         
                        
                        </TouchableWithoutFeedback>
                            <Text style={[styles.PoppinThin , {fontSize:10}]}>Label Foto</Text>
                            <Text style={[styles.PoppinBold , {fontSize:10}]}>{this.state.labelFoto}</Text>
                            <Text style={[styles.PoppinRegular , {fontSize:10, color:'#f79220'}]}>
                            {
                                this.state.labelFoto ? 'Hapus' : null
                            }
                            </Text>
                   
                    </View>

                    <View style={styles.circleButtonContainer}>
                        <TouchableWithoutFeedback onPress={() => this._toggleModal('datepicker')}>
                                {
                                    this.state.datePicked ? 
                                    <View style={styles.circleButton}>

                                        <Image style={[styles.circleButtonIcon, {backgroundColor:'#29ba9b', tintColor:'white'}]}
                                        source={require('../../../assets/images/waktuKadaluarsa.png')}/> 
                                    
                                    </View>
                                    
                                    
                                :   <View style={styles.circleButton}>
                            
                                        <Image style={styles.circleButtonIcon}
                                        source={require('../../../assets/images/waktuKadaluarsa.png')}/>         
                        
                                    </View>
                                }
                            
                            
                            </TouchableWithoutFeedback>

                        

                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Kadaluarsa</Text>
                        <Text style={[styles.PoppinBold , {fontSize:10 , color:'#29ba9b'}]}>24:00</Text>
                        <Text style={[styles.PoppinBold , {fontSize:10}]}>22 Des 2017</Text>
                        <Text style={[styles.PoppinRegular , {fontSize:10, color:'#f79220'}]}>Hapus</Text>

                    </View>

                    <View style={styles.circleButtonContainer}>

                        <View style={styles.circleButton}>
                            
                            <Image style={styles.circleButtonIcon}
                            source={require('../../../assets/images/forms.png')}/>
                        
                        </View>

                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Respon</Text>
                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Belum Ada</Text>

                    </View>

				</View>

				<View style={styles.buttonContainer}>
                    
                    <View style={styles.circleButtonContainer}>

                        <View style={styles.circleButton}>
                            <Image style={styles.circleButtonIcon}
                            source={require('../../../assets/images/hakAkses.png')}/>
                        </View>

                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Hak akses</Text>
                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Responden</Text>
                        <Text style={[styles.PoppinBold , {fontSize:10}]}>Edit</Text>
                    
                    </View>

                    <View style={styles.circleButtonContainer}>

                        <TouchableOpacity onPress={() => this.setState({verifikasi:!this.state.verifikasi})}
                        style={this.state.verifikasi ? styles.circleButtonActive : styles.circleButton}>
                            
                            <Image style={this.state.verifikasi ? styles.circleButtonIconActive : styles.circleButtonIcon}
                            source={require('../../../assets/images/verifikasi.png')}/>
                        
                        </TouchableOpacity>

                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Sifat Respon</Text>
                        <Text style={[styles.PoppinBold , {fontSize:10}]}>
                                
                                {this.state.verifikasi ? 'Perlu Verifikasi' : 'Tanpa Verifikasi'}
                            
                        </Text>

                    </View>
                    <View style={styles.circleButtonContainer}>

                        <TouchableOpacity onPress={() => this.setState({acak:!this.state.acak})}
                        style={this.state.acak ? styles.circleButtonActive : styles.circleButton}>
                            
                            <Image style={this.state.acak ? styles.circleButtonIconActive : styles.circleButtonIcon }
                            source={require('../../../assets/images/shuffle.png')}/>
                        
                        </TouchableOpacity>

                        <Text style={[styles.PoppinThin , {fontSize:10}]}>Acak</Text>
                        <Text style={[styles.PoppinBold , {fontSize:10}]}>Pertanyaan</Text>

                    </View>                    
				</View> 

			</View>

			<View style={styles.formFooter}>
    
                <TouchableWithoutFeedback onPress={ () => navigate ('PostingFormPertanyaan')}>
                <View style={styles.button}>
                    <Text style={[styles.PoppinSemiBold , {fontSize:16, color:'#fff'}]}>Lanjutkan</Text>
                </View>
                </TouchableWithoutFeedback>
			
            </View>

            <Modal isVisible={this.state.isModalVisible}
            onBackdropPress={() => this.setState({ isModalVisible: false })}>

                {this.state.modalID == 'camera' && <ModalPilihGambar receiveData={this.receiveData}/>}
        
                {this.state.modalID == 'datepicker' && <ModalDatepicker modalHandlePress={(id) => this._modalHandlePress(id)}/>}
                

            </Modal>
        
        </View>
         
	}
}

const globalStyles = (require('../../../assets/styles/global'));

const styles = Object.assign({}, globalStyles, StyleSheet.create({
    
    formHeader:{
        flex: 1.5,
    },

    formHeaderTextInput:{
        flex:1,
        marginLeft: 5,
        marginRight: 5,
    },

    formContainer:{
        flex: 8.5,
        margin: 5,
        flexDirection: 'column',
    },

    buttonContainer:{
        flexDirection: 'row',
    },
    
    circleButtonContainer:{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },

    circleButton:{
        width: 50,
        height: 50,
        marginTop:12,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#848285',
        borderRadius: 100,
    },

    circleButtonActive:{
        width: 50,
        height: 50,
        marginTop:12,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#29ba9b',
        borderWidth: 1,
        borderColor: '#848285',
        borderRadius: 100,
    },

    circleButtonIcon:{
        width:20,
        height: 20,
    },

    circleButtonIconActive:{
        width:20,
        height: 20,
        tintColor:'white',
    },

    

    labelImage:{
        flex:1,
        borderRadius:100,
        alignSelf: 'stretch'
    },

    formFooter:{
        flex: 1,
        margin: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    
    button:{
        borderRadius: 20,
        backgroundColor: '#f79220',
        padding: 16,
        justifyContent: 'center',
    },


}))
