// Author : Rifqi A


import React, { Component } from 'react';
import { 
	View, 
	Text, 
	StyleSheet, 
	Image, 
	Button, 
	TouchableOpacity,
	TextInput,
	Animated,
	ScrollView
} from 'react-native';
 

import CardPertanyaan from '../../components/CardPertanyaan';


export default class PostingFormScreenPertanyaan extends Component {
	
	constructor(props) {
	
		super(props);

		this.state = { 

			valueArray: [<CardPertanyaan/>,], 

			disabled: false 

		}

        this.index = 0;
	}

	addCard = () =>{
	
		let temp = this.index ++
	
		this.state.valueArray.push(temp)
	
		this.setState({
	
			valueArray: this.state.valueArray
	
		});
		
	}

	render() {

		const { navigate } = this.props.navigation;

		let newArray = this.state.valueArray.map((item, key) => 
		{
			return <CardPertanyaan key={key}/>
				
		});

		return(
			<View style={styles.container}>
				<ScrollView>
					
					{
						newArray
					}

					<View style={styles.container2}>
						
						<TouchableOpacity onPress={this.addCard}
						style={styles.buttonTambah}>
							
							<Image style={{width:20, height:20}}
							source={require('../../../assets/images/add.png')}/>
							
							<Text style={[styles.PoppinSemiBold, {marginLeft:4, fontSize:16,}]}>Tambah</Text>

						</TouchableOpacity>
					
					</View>
					
					<View style={styles.container3}>

						<TouchableOpacity style={styles.buttonPreview} onPress={() => navigate('PostingFormScreenPreview')}>

							<Text style={[styles.PoppinSemiBold, {fontSize:16}]}>PREVIEW FORM</Text>

						</TouchableOpacity>

						<View style={{flexDirection:'row'}}>

							<TouchableOpacity onPress={() => this.props.navigation.goBack()}
							style={[styles.buttonFooterStyle, {backgroundColor:'#848285'}]}>
							
								<Text style={styles.textFooter}>KEMBALI</Text>

							</TouchableOpacity>

							<TouchableOpacity style={[styles.buttonFooterStyle, {backgroundColor :'#f79220'}]}>
							
								<Text style={styles.textFooter}>BUAT FORM</Text>

							</TouchableOpacity>		
						
						</View>
					
					</View>
				
				</ScrollView>

			</View>
			
			
		);
	}
}

const globalStyles = require ('../../../assets/styles/global');

const styles = Object.assign ( {}, globalStyles,
	StyleSheet.create ({
		container2:{
			flex:1,
			marginLeft: 5,
			marginRight: 5,
			marginBottom:10,
		},

		buttonTambah:{
			flex:1,
			flexDirection:'row',
			justifyContent: 'center',
			alignItems: 'center',
			borderRadius: 10,
			marginTop: 12,
			marginLeft:15,
			marginRight:15,
			borderColor: '#848285',
			elevation:1,
		},

		emptyContainer:{
			flex:3,
		},

		container3:{
			flex:2,
			marginLeft:10,
			marginRight:10,
			flexDirection:'column',
			justifyContent: 'flex-end',
		},

		buttonPreview:{
			borderRadius: 50,
			justifyContent: 'center',
			alignItems:'center',
			borderColor: '#848285',
			marginBottom:4,
			elevation:1,
		},

		buttonFooterStyle:{
			flex:1,
			padding:6,
			justifyContent: 'center',
			alignItems:'center',
			borderRadius:50,
			marginTop:10,
			marginBottom:10,
		},
		
		textFooter:{
			fontSize:16,
			color: 'white',
			fontFamily: 'PoppinBold',
		},

		viewHolder:{
			height: 55,
			backgroundColor: '#26A69A',
			justifyContent: 'center',
			alignItems: 'center',
			margin: 4
		},

		text:{
			color: 'white',
			fontSize: 25
		},

  })
);

