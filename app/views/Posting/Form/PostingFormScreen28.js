import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    TouchableOpacity
} from 'react-native';

export default class PostingForm28 extends Component {
    render() {
        return(
            <View style={styles.main}>
                <View style={styles.container}>
                    <View style={styles.image}>
                        <Image style={styles.dot3Image} source={require('../../../assets/images/3dot.png')}/>
                    </View>
                    <TextInput style={styles.input} 
                        placeholder = "Nama Anda"
                        underlineColorAndroid = 'grey'
                    />        
                    <Text style={styles.green}>
                        Jawaban Singkat
                    </Text>    
                    <TextInput style={styles.input2} 
                        placeholder = "Tuliskan Nama Lengkap Anda" 
                        underlineColorAndroid = '#29B99B'
                    />
                    <TextInput style={styles.input3}
                        placeholder = "Jawaban Singkat"
                        underlineColorAndroid = '#C2C1C3'
                    />
                        <View style={styles.input4}>
                            <TextInput
                                underlineColorAndroid = '#C2C1C3'
                            />
                                <View style={styles.image2}>
                                    <Image style={styles.dotImage}
                                        source={require('../../../assets/images/dot.png')}/>
                                    <Text style={styles.orange}> 
                                        Wajib Diisi
                                    </Text>
                                </View>            
                        </View>
                </View>

                <View>
                    <TouchableOpacity style={styles.button}>
                    <View style={styles.image3}>   
                        <Image style={styles.plusImage}
                            source={require('../../../assets/images/plus-math.png')}/>                  
                        <View style={styles.buttonText}>
                            <Text style={styles.buttonText2}>
                                Tambah
                            </Text>
                        </View>
                    </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    container: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#C2C1C3',
        margin: 6
    },
    input: {
        marginLeft: 25,
        marginRight: 25,
        fontSize: 18,
        fontWeight: 'bold'
    },
    button: {
        borderRadius: 10,
        borderWidth: 1,
        marginLeft: 25,
        marginRight: 25,
        marginTop: 9,
        padding: 8,
        borderColor: '#C2C1C3',
    },
    input2: {
        marginLeft: 25,
        marginRight: 25,
        fontSize: 16
    },
    green: {
        borderRadius: 25,
        borderWidth: 0.5,
        backgroundColor: '#29B99B',
        marginRight: 200,
        marginLeft: 25,
        marginBottom: 12,
        borderColor: 'transparent',
        textAlign: 'center',
        color: 'white',
        padding: 3
    },
    input3: {
        marginBottom: 20,
        marginLeft: 25,
        fontSize: 16,
        marginRight: 75
    },
    input4: {
        marginRight: 25,
        marginLeft: 25,
        marginBottom: 14
        
    },
    orange: {
        textAlign: 'right'
    },
    dot3Image:{
        width: 20,
        height: 20,
        marginTop: 10,
        marginRight:20
    },
    plusImage: {
        width: 30,
        height: 30
    },
    dotImage: {
        width: 20,
        height: 20,
        marginRight: 10
    },
    image: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    image2: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    image3: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    buttonText: {
        marginTop: 4
    },
    buttonText2: {
        color: 'black'
    }
});