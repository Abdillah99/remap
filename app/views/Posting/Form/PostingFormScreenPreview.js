import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Button,
  Image,
} from 'react-native';

export default class PostingFormScreenPreview extends Component{


render() {
 	return (
		<View style={styles.container}>
 
			<View style={styles.eventCardContainer}>
				
				<View style={styles.eventCardHeaderContainer}>

					<View style={styles.eventCardHeaderPhoto}>

						<Image style={{width:50, height:50, borderRadius:100, borderWidth:1, borderColor:'tomato'}} 
						source={require('../../../assets/images/user.png')}/>

					</View>

					<View style={styles.eventCardHeaderDesc}>

						<View style={styles.eventCardIcon}>
							
							<Image style={{height:16, width:16}}
							source={require('../../../assets/images/waktuKadaluarsa.png')}/>

							<Image style={{height:16, width:16}}
							source={require('../../../assets/images/file.png')}/>

						</View>

						<View style={styles.eventCardIsi}>
							
							<View style={{flex:1, flexDirection:'row'}}>
							
								<Text style={[styles.PoppinSemiBold, {alignSelf:'center'}]}>24:59 ●</Text>
								<Text style={[styles.PoppinRegular, {alignSelf:'center'}]}> 22 Des 2017</Text>
								
							</View>
							
							<View style={{flex:1, flexDirection:'row'}}>

								<Text style={[styles.PoppinRegular, {color:'#29ba9b', alignSelf:'center'}]}>0 </Text>
								<Text style={[styles.PoppinRegular, {alignSelf:'center'}]}>Respon</Text>

							</View>

						</View>

					</View>
				
				</View>

				<View style={styles.eventCardFooterContainer}>
					
					<Text style={[styles.PoppinRegular, {fontSize: 12}]}>Form</Text>
					<Text style={[styles.PoppinBold, {fontSize: 17, color:'black'}]}>REG CONCERT TOUR 2018</Text>
					<Text style={[styles.PoppinLight, {fontSize:14, color:'#f79220' }]}>* Wajib diisi</Text>
				
				</View>
			
			</View>

			<View style={styles.formContainer}>
				
				<View style={styles.formCardContainer}>
				
					<View style={{flexDirection:'row', borderColor:'#848285',borderBottomWidth:0.5}}>
						
						<Text style={[styles.PoppinSemiBold, {fontSize:16}]}>Nama Anda </Text>
						<Text style={{color:'#f79220'}}> * </Text>

					</View>
						
					<TextInput placeholder="Tuliskan Nama Lengkap Anda"/>	

					<TextInput placeholder="Tulis Jawaban"/>

				</View>

				<View style={styles.formCardContainer}>
				
					<View style={{flexDirection:'row', borderColor:'#848285',borderBottomWidth:0.5}}>
					
						<Text style={[styles.PoppinSemiBold, {fontSize:16}]}>Unggah Foto Anda </Text>
						<Text style={{color:'#f79220'}}> * </Text>

					</View>

					<View style={{justifyContent:'flex-start', flexDirection:'row', marginTop:10,}}>

						<TouchableOpacity style={styles.buttonStyle}>
						
							<Image style={{width:16, height:16, tintColor:'white'}} 
							source={require('../../../assets/images/upload.png')}/>
							
							<Text style={[styles.PoppinSemiBold, {color:'white', marginLeft:4, fontSize:10}]}>Unggah File</Text>
						
						</TouchableOpacity>
					
					</View>

				</View>

				<View style={{flex:1, flexDirection:'column', justifyContent:'flex-end'}}>

					<TouchableOpacity style={{backgroundColor:'#f79220', borderRadius:50, padding:8, margin:5}}>
					
						<Text style={[styles.PoppinSemiBold, {color:'white', alignSelf:'center'}]}>Tutup Preview</Text>
					
					</TouchableOpacity>
			
				</View>

			</View>
		
		</View>
   )
  }
}

const globalStyles = require ('../../../assets/styles/global');

const styles = Object.assign ( {}, globalStyles,
	StyleSheet.create ({
		eventCardContainer:{
			flex:3,
			backgroundColor: '#f7f7ff',
			flexDirection: 'column',
			elevation:3,
		},

		eventCardHeaderContainer:{
			flex:1,
			flexDirection:'row',
			justifyContent: 'center',
			alignContent: 'center',
		},

		eventCardHeaderPhoto:{
			flex:1,
			justifyContent: 'center',
			alignItems: 'center',
		},

		eventCardHeaderDesc:{
			flex:4,
			flexDirection: 'row',
			marginTop: 10,
		},
		
		eventCardIcon:{
			flex:1,
			flexDirection: 'column',
			justifyContent: 'space-around',
			alignItems:'center',
		},

		eventCardIsi:{
			flex:6,
			flexDirection: 'column',
		},

		eventCardFooterContainer:{
			flex:1,
			flexDirection:'column',
			marginLeft: 20,
		},

		formContainer:{
			flex: 7,
			padding: 8,
			flexDirection:'column'
		},

		formCardContainer:{
			borderRadius:10,
			flexDirection:'column',
			padding:14,
			borderWidth:0.5,
			borderColor:'#848285',
			margin:12,
		},
		buttonStyle:{
			justifyContent: 'center',
			borderRadius:50, 
			backgroundColor:'#29ba9b', 
			flexDirection:'row', 
			paddingTop: 3,
			paddingBottom: 3,
			paddingLeft: 4,
			paddingRight: 4,
		}

  })
);

