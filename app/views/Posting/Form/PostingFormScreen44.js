import React, { Component } from 'react';
import {
	AppRegistry,
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	Image,
	View
} from 'react-native';


export default class PostingForm44 extends Component {
	render () {
		return (
				<View style={ styles.container1 }>
					<View style={ styles.container2 }>
						<View style={ styles.headerContainer2 }>
							<View style={styles.iconTitle}>
					            <Image source={require('../../../assets/images/uvu.jpg')} style={styles.icon} />
					            	<View style={styles.gaweText}>
						          		<Text style={styles.text1}>Form </Text>
						          		<Text style={styles.text2}>REG CONCERT TOUR 2018 </Text>
					        			<Text style={styles.text3}>Diposting oleh Username </Text>
					        		</View>
					        		<View style={styles.sebelaheText}>
					        			<Text style={styles.text1}>1 detik lalu</Text>
					        			<Image source={require('../../../assets/images/more.png')} style={styles.iconMini}></Image>
					        		</View>
					        </View>
						</View>
						<View style={styles.bawahHeader}>
			          		<View style={styles.tulisanButton}>
			          			<TouchableOpacity style={styles.button}>
			          				<Image source={require('../../../assets/images/text.png')} style={styles.iconButton} />
				            	</TouchableOpacity>
			          			<Text style={styles.tulisannya}>Respon </Text>
			          			<Text style={styles.tulisannya1}>Belum Ada </Text>
			          		</View>
			          		<View style={styles.tulisanButton}>
			          			<TouchableOpacity style={styles.button}>
			          				<Image source={require('../../../assets/images/stopwatch.png')} style={styles.iconButton} />
				            	</TouchableOpacity>
			          			<Text style={styles.tulisannya}>Kadaluarsa </Text>
			          			<Text style={styles.tulisannya2}>24:59 </Text>
			          			<Text style={styles.tulisannya3}>22 Des 2017 </Text>
			          		</View>
			          		<View style={styles.tulisanButton}>
			          			<TouchableOpacity style={styles.button}>
			          				<Image source={require('../../../assets/images/select.png')} style={styles.iconButton}/>
				           		 </TouchableOpacity>
			          			<Text style={styles.tulisannya}>Respon </Text>
			          			<Text style={styles.tulisannya03}>Daftar </Text>
			          			<Text style={styles.tulisannya03}>Sekarang </Text>
			          		</View>
			          	</View>
					</View>
					<View style={ styles.container3 }>
						<View style={styles.dalamContainer3}>
							<Text style={styles.tulisanDalamCont3}>Attention Please....!!! this is a dummy text you should know is a dummy dummy text, a dummy dummy text</Text>
							<View style={styles.feture}>
								<View style={styles.feture1}>
									<Text style={styles.fetureAngka}>0</Text>
									<Text style={styles.fetureText}>Suka</Text>	
								</View>
								<View style={styles.feture2}>
									<Text style={styles.fetureAngka1}>0</Text>
									<Text style={styles.fetureText1}>Bagikan</Text>
								</View>
								<View style={styles.feture3}>
									<Text style={styles.fetureAngka2}>0</Text>
									<Text style={styles.fetureText2}>Komentar</Text>	
								</View>
							</View>
							<View style={styles.bawahFeture}>
								<View styles={styles.iconFeture}>
									<TouchableOpacity style={styles.buttonFeture}>
			          					<Image source={require('../../../assets/images/folder.png')} style={styles.iconKecil} />
				            		</TouchableOpacity>
								</View>
								<View styles={styles.iconFeture1}>
									<TouchableOpacity style={styles.buttonFeture1}>
			          					<Image source={require('../../../assets/images/broken-heart.png')} style={styles.iconKecil} />
				            		</TouchableOpacity>
								</View>
								<View styles={styles.iconFeture2}>
									<TouchableOpacity style={styles.buttonFeture2}>
			          					<Image source={require('../../../assets/images/like.png')} style={styles.iconKecil} />
				            		</TouchableOpacity>
								</View>
								<View styles={styles.iconFeture3}>
									<TouchableOpacity style={styles.buttonFeture3}>
			          					<Image source={require('../../../assets/images/share.png')} style={styles.iconKecil} />
				            		</TouchableOpacity>
								</View>
							</View>
						</View>
					</View>
				</View>

			
			);
	}
}

const styles = StyleSheet.create({
	container1: {
		flex: 0.65,
		backgroundColor: 'white',
		borderWidth: 0.5,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		borderBottomRightRadius: 10,
		borderBottomLeftRadius: 10,
		margin: 10,
	},
	
	container2: {
		flex: 0.7,
		backgroundColor: '#F8F3F7',
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		borderLeftWidth: 10, 
		borderLeftColor: '#33c68e',
	},

	container3: {
		flex: 0.5,
		backgroundColor: 'white',
		borderBottomRightRadius: 10,
		borderBottomLeftRadius: 10,
	},

	headerContainer2: {
	    flexDirection: 'row',
  	},

  	dalamContainer3: {
  		flexDirection: 'column',
  		flex: 1,
  	},

  	bawahHeader: {
  		flexDirection: 'row',
  		marginTop: 17,
  		marginLeft: 30,
  	},

  	bawahFeture: {
  		flexDirection: 'row',
  		justifyContent: 'flex-start',
  		marginTop: 10,
  		marginLeft: 10,
  		marginRight: 10,
  		marginBottom: 10,
  	},

  	tulisanButton: {
  		flexDirection: 'column',
  		alignItems: 'center',
  		justifyContent: 'flex-start',
  		flex: 1,
  	},

  	tulisannya: {
  		fontSize: 15,
  		color: '#BAB8BB',
  	},

  	tulisannya1: {
  		fontSize: 15,
  		color: '#E9E6EB',
  	},

  	tulisannya2: {
  		fontSize: 16,
  		fontWeight: 'bold',
  		color: '#28e0af',
  	},

  	tulisannya3: {
  		fontSize: 15,
  		fontWeight: 'bold',
  		color: '#6C6D6C',
  	},

  	tulisannya03: {
  		fontSize: 15,
  		fontWeight: 'bold',
  		color: '#6C6D6C'
  	},

  	tulisanDalamCont3: {
  		fontSize: 17,
  		color: 'black',
  		marginLeft: 10,
  		marginTop: 10,
  		marginRight: 10,
  	},

  	iconTitle:{
	    flexDirection: 'row',
	    flex: 1,
  	},

  	gaweText: {
  		flexDirection: 'column',
  		flex: 1,
  		marginLeft: 10,

  	},

  	sebelaheText: {
  		flex: 'column',
  		flex: 0.3,
  		marginRight: 10,
  	},
  	
  	icon: {
	    height: 60,
	    width: 60,
	    marginLeft: 15,
	    marginTop: 10,
	    borderRadius: 100,
	    borderWidth: 1,
	    borderColor: '#dbdbdb',
  	},

  	iconMini: {
  		height: 25,
	    width: 25,
	    alignItems: 'center',
  		justifyContent: 'center',
  		marginLeft: 20,
  	},

  	iconButton: {
	    height: 32,
	    width: 32,
	    alignItems: 'center',
  		justifyContent: 'center',
  	},

  	iconKecil: {
  		height: 30,
	    width: 30,
	    alignItems: 'center',
  		justifyContent: 'center',
  	},

  	iconKecil1: {
  		height: 30,
	    width: 30,
  	},

  	iconKecil2: {
  		height: 30,
	    width: 30,
  	},

  	iconKecil3: {
  		height: 30,
	    width: 30,
  	},

  	iconFeture: {
  		width: 30,
  		height: 30,
  	},

  	iconFeture1: {
  		width: 30,
  		height: 30,
  	},

  	iconFeture2: {
  		width: 30,
  		height: 30,

  	},

  	iconFeture3: {
  		width: 30,
  		height: 30,
  	},

  	text1: {
  		marginTop: 10,
  		color: '#B6B5B8',
  		fontSize: 13,
  	},

  	text2: {
  		color: '#000',
  		fontSize: 18,
  		fontWeight: 'bold',
  	},

  	text3: {
  		color: '#B6B5B8',
  		fontSize: 12,
  	},

  	button: {
  		width: 55,
  		height: 55,
  		borderRadius: 100,
  		borderWidth: 1,
  		borderColor: '#dbdbdb',
  		backgroundColor: '#fff',
  		alignItems: 'center',
  		justifyContent: 'center',
  		marginBottom: 10,
  	},

  	button2: {
  		width: 55,
  		height: 55,
  		borderRadius: 100,
  		borderWidth: 1,
  		borderColor: '#dbdbdb',
  		backgroundColor: '#29BA9B',
  		alignItems: 'center',
  		justifyContent: 'center',
  		marginBottom: 10,
  	},

  	buttonFeture: {
  		width: 32,
  		height: 32,
  		alignItems: 'center',
  		marginLeft: 5,
  	},

  	buttonFeture1: {
  		width: 32,
  		height: 32,
  		alignItems: 'center',
  		marginLeft: 20,
  	},

  	buttonFeture2: {
  		width: 32,
  		height: 32,
  		alignItems: 'center',
  		marginLeft: 20,
  	},

  	buttonFeture3: {
  		width: 32,
  		height: 32,
  		alignItems: 'center',
  		marginLeft: 185,
  	},

  	feture: {
  		flexDirection: 'row',
  		flex: 1,
  		marginTop: 30,
  		marginLeft: 10,
  		marginRight: 10,
  	},

  	feture1: {
  		flexDirection: 'row',
  		flex: 0.1,
  	},

  	feture2: {
  		flexDirection: 'row',
  		flex: 0.2,
  	},

  	feture3: {
  		flexDirection: 'row',
  		flex: 0.5,
  		marginLeft: 31,
  	},

  	fetureAngka: {
  		marginLeft: 5,
  		fontSize: 14,
  	},

  	fetureText: {
  		marginLeft: 5,
  		fontSize: 14,
  	},

  	fetureAngka1: {
  		marginLeft: 20,
  		fontSize: 14,
  	},

  	fetureText1: {
  		marginLeft: 5,
  		fontSize: 14,
  	},

  	fetureAngka2: {
  		marginLeft: 150,
  		fontSize: 14,
  	},

  	fetureText2: {
  		marginLeft: 5,
  		fontSize: 14,
  	},

})